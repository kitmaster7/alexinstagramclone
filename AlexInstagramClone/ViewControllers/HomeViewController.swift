//
//  HomeViewController.swift
//  AlexInstagramClone
//
//  Created by alex on 23.09.2018.
//  Copyright © 2018 alex. All rights reserved.
//

import UIKit
import FirebaseAuth
class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func LogoutButton(_ sender: Any) {

        do {
            try Auth.auth().signOut()
        } catch {
        }

        print(Auth.auth().currentUser)
        do{
            try Auth.auth().signOut()
        }catch let logoutError{
            print(logoutError)
        }
           print(Auth.auth().currentUser)


        let storyboard = UIStoryboard(name: "Start", bundle: nil)
        let signInVC = storyboard.instantiateViewController(withIdentifier: "SignInViewController")
        self.present(signInVC, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
