//
//  SignUpViewController.swift
//  AlexInstagramClone
//
//  Created by alex on 23.09.2018.
//  Copyright © 2018 alex. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
class SignUpViewController: UIViewController {
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    
    var selectedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = 30
        profileImage.clipsToBounds = true
        profileImage.layer.backgroundColor = UIColor.black.cgColor
        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.handleSelectProfileImageView))
        profileImage.addGestureRecognizer(tapGesture)
        profileImage.isUserInteractionEnabled = true
        signUpButton.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
             signUpButton.isEnabled = false
        handleTextField()
    }
    
    func handleTextField(){
        userNameTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        emailTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        passwordTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(){
        // print(userNameTextField.text)
        guard
            let username = userNameTextField.text, !username.isEmpty,
            let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text,!password.isEmpty
            else {
                signUpButton.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
                signUpButton.isEnabled = false
                return
        }
        signUpButton.setTitleColor(UIColor.blue, for: UIControl.State.normal)
        signUpButton.isEnabled = true
        
        
    }
    
    
    @objc func handleSelectProfileImageView(){
        print("tapped")
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        present(pickerController, animated: true, completion: nil)
        
        
    }
    
    fileprivate func setUserInformation(profileImageUrl:String, username:String, email:String, uid: String) {
        let ref = Database.database().reference()
        let usersReference = ref.child("users")
        let newUserRef = usersReference.child(uid);                 newUserRef.setValue(["username":username,"email":email,"profileImageUrl":profileImageUrl])
        
        self.performSegue(withIdentifier: "signUpTabBar", sender: nil)
        
    }
    
    @IBAction func signUp_TouchInside(_ sender: Any) {
        
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error != nil {
                print("HERE IS AN ERROR")
                print(error!.localizedDescription)
                print("END OF ERROR")
                return
            }
            let uid = user?.user.uid
            let storageRef = Storage.storage().reference(forURL: "gs://instagramclone-d3402.appspot.com").child("profile_image")
                .child(uid!)
            if let profileImg = self.selectedImage, let imageData = profileImg.jpegData(compressionQuality: 0.1){
                storageRef.putData(imageData, metadata: nil, completion:{(metadata,error) in
                    if error != nil{
                       print("ERROR BEFORE PUT DATA")
                        print(error?.localizedDescription)
                          print("ERROR END PUT DATA")
                        return
                    }
                    let profileImageUrl = metadata?.path
                    self.setUserInformation(profileImageUrl: profileImageUrl!,username: self.userNameTextField.text!,email: self.emailTextField.text!, uid: uid!)
                    
       
                })
            }
            
            
        }
       
        
        
        
        
        
        
    }
    @IBAction func dismiss_onClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
    */

}


extension SignUpViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
    
    
        print("Finish")
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            selectedImage = image
            profileImage.image = image
        }
        print(info)
        //profileImage.image = InfoPhoto
        
        dismiss(animated: true, completion: nil)
    }
}
