//
//  SignInViewController.swift
//  AlexInstagramClone
//
//  Created by alex on 23.09.2018.
//  Copyright © 2018 alex. All rights reserved.
//

import UIKit
import FirebaseAuth
class SignInViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
   
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    
    @IBAction func signInButton_Touch(_ sender: Any) {
       AuthService.signIn()
     
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.backgroundColor = UIColor.clear
        emailTextField.attributedPlaceholder = NSAttributedString(string:emailTextField.placeholder!)
        let bottomLayer = CALayer()
        bottomLayer.frame = CGRect(x: 0, y: 29, width: 100, height: 0.6)
        bottomLayer.backgroundColor = UIColor.black.cgColor
        emailTextField.layer.addSublayer(bottomLayer)
        signInButton.isEnabled = false
        signInButton.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        handleTextField()
        
        
        
    }
    
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         if (Auth.auth().currentUser != nil) {
        self.performSegue(withIdentifier: "signIntoTabBar", sender: nil)
        }
    }
    

    func handleTextField(){
        emailTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        passwordTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
    }
    
    
    
    @objc func textFieldDidChange(){
        // print(userNameTextField.text)
        guard
            let email = emailTextField.text,
           !email.isEmpty,
            let password = passwordTextField.text,!password.isEmpty
            else {
                signInButton.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
                signInButton.isEnabled = false
                return
        }
        signInButton.setTitleColor(UIColor.blue, for: UIControl.State.normal)
        signInButton.isEnabled = true
        
        
    }

}
