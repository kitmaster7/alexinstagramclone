//
//  AuthService.swift
//  AlexInstagramClone
//
//  Created by alex on 10.10.2018.
//  Copyright © 2018 alex. All rights reserved.
//

import Foundation
import FirebaseAuth
class AuthService{
    
   // static var shared = AuthService()
    
    static func signIn(){
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (authDataResult, error) in
            if error != nil{
                print(error!.localizedDescription)
                return
            }
            
            self.performSegue(withIdentifier: "signIntoTabBar", sender: nil)
            print(authDataResult?.user.email)
        })
        
    }
    
}
